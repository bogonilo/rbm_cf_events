import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore

# Use the application default credentials
cred = credentials.Certificate('/Users/lorenzo.bogoni/Desktop/Thesis/implementation/credentials/connective-dev-firebase-adminsdk-26tap-dfd660eca4.json')
firebase_admin.initialize_app(cred)

db = firestore.client()

doc_ref = db.collection('users').stream()

for doc in doc_ref:
    print('{} => {}'.format(doc.id, doc.to_dict()))