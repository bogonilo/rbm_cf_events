import tensorflow.compat.v1 as tf
#Numpy contains helpful functions for efficient mathematical calculations
import numpy as np
#Dataframe manipulation library
import pandas as pd
#Graph plotting library
import matplotlib.pyplot as plt

pd.options.display.width = 0
tf.disable_v2_behavior()

relationships_df = pd.read_csv('data/data_csv/ue_relationships.csv', sep=':', header=None, engine='python')
ratings_df = pd.read_csv('data/data_csv/ue_participations.csv', sep=':', header=None, engine='python')
events_df = pd.read_csv('data/data_csv/events.csv', sep=':', header=None, engine='python')
users_df = pd.read_csv('data/data_csv/users.csv', sep=':', header=None, engine='python')
relationships_df.columns = ['UserID', 'EventID', 'Participation', 'Relationship']
ratings_df.columns = ['UserID', 'EventID', 'Participation']
events_df.columns = ['EventID', 'OrganizerID', 'OfficeTarget', 'Categories']
users_df.columns = ['UserID', 'Office', 'Chapter', 'Interests']

user_participation_df = ratings_df.pivot(index='UserID', columns='EventID', values='Participation')
user_relationship_df = relationships_df.pivot(index='UserID', columns='EventID', values='Relationship')
print(user_participation_df.head(5))
print(user_relationship_df.head(5))

trX = user_participation_df.values
index = user_participation_df.index
rel_matrix = user_relationship_df.values

# print(np.array(rel_matrix[0]))

def rel_array_to_values(rel_array):
    vector = []
    for item in rel_array:
        values = item.split(',')
        value = (float(values[0]) + float(values[1]) + float(values[2]) + float(values[3])) / 4.0
        vector.append(value)
    return vector

# changing relationship matrix with normalized values
for i in range(len(rel_matrix)):
    rel_matrix[i] = rel_array_to_values(np.array(rel_matrix[i]))
# print(rel_matrix.shape, tf.transpose(rel_matrix).shape)

result = (rel_matrix + trX) / 2.0

# def calculate_relationships(user_id):
#
# def get_relationships(user_id):
