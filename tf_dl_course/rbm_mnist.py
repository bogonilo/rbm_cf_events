import tensorflow.compat.v1 as tf
import numpy as np
import tensorflow.keras.datasets.mnist as mnist
from PIL import Image
# from .utils import tile_raster_images
import matplotlib.pyplot as plt

tf.disable_v2_behavior()

(trX, trY), (teX, teY) = mnist.load_data(path='mnist.npz')

print(trX.shape, trY.shape, teX.shape, teY.shape)