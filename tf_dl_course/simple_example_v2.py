import pandas as pd
import numpy as np
import tensorflow as tf

df = pd.read_csv('FuelConsumptionCo2.csv')

print(df.head(5).to_string())

""" Converting df column to array of data as tf.constants"""
_x = tf.constant(np.asanyarray(df[['ENGINESIZE']]), dtype=tf.float32)
_y = tf.constant(np.asanyarray(df[['CO2EMISSIONS']]), dtype=tf.float32)

@tf.function
def multiply(a, b):
    return tf.math.multiply(a, b)

print(multiply(_x, _y))
