import tensorflow.compat.v1 as tf
import numpy as np

tf.disable_v2_behavior()

a = tf.constant([2])
b = tf.constant([3])

c = tf.add(a,b)

init = tf.global_variables_initializer()
session = tf.Session()

result = session.run(c)

# print(result)

session.close()

epochs = 15
batchsize = 100
errors = []
# trX = [0.1, 0.3, 0.34, 0.05]
trX = np.ones(300)
print(range(0, len(trX), batchsize))
print(range(batchsize, len(trX), batchsize))
print(list(zip( range(0, len(trX), batchsize), range(batchsize, len(trX), batchsize), range(300))))
# for i in range(epochs):
# for start, end in zip( range(0, len(trX), batchsize), range(batchsize, len(trX), batchsize)):
#     print(str(3009080778) +', '+ str(start)+', '+ str(end)+ '. ')