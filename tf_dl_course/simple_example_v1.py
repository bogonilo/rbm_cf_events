import pandas as pd
import numpy as np
import tensorflow.compat.v1 as tf

tf.disable_v2_behavior()

df = pd.read_csv('FuelConsumptionCo2.csv')

print(df.head(5).to_string())

""" Converting df column to array of data """
_x = np.asanyarray(df[['ENGINESIZE']])
_y = np.asanyarray(df[['CO2EMISSIONS']])

"""Building the graph (default graph)"""
a = tf.placeholder(tf.float32, [len(_x), 1])
b = tf.placeholder(tf.float32, [len(_y), 1])

mul = tf.math.multiply(a,b)

"""creating the session where to run the graph"""
sess = tf.Session()

result = sess.run(mul, feed_dict={a: _x, b: _y})

print(result)
