import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import tensorflow.compat.v1 as tf
import matplotlib.patches as mpatches
plt.rcParams['figure.figsize'] = (10, 6)

tf.disable_v2_behavior()

df = pd.read_csv('FuelConsumptionCo2.csv')

print(df.head())

""" Converting df column to array of data """
train_x = np.asanyarray(df[['ENGINESIZE']])
train_y = np.asanyarray(df[['CO2EMISSIONS']])

""" Initializing a and b at any initial random guess"""
a = tf.Variable(20.0)
b = tf.Variable(30.2)

y = a * train_x + b

"""equation to be minimized which in this case is the loss
    calculated as mean of the squared error"""
loss = tf.reduce_mean(tf.square(y - train_y))

""" Gradient descent optimizer """
# optimizer = tf.keras.optimizers.SGD(0.05)
optimizer = tf.train.GradientDescentOptimizer(0.06)

""" Telling the optimizer that it has to minimize the loss function """
train = optimizer.minimize(loss)

""" IN TF1 this initializes all the variables """
init = tf.global_variables_initializer()
sess = tf.Session()
sess.run(init)

loss_values = []
train_data= []

"""here the actual execution begins"""
for step in range(100):
    _, loss_val, a_val, b_val = sess.run([train, loss, a, b])
    loss_values.append(loss_val)
    if step % 5 == 0:
        print(step, loss_val, a_val, b_val)
        train_data.append([a_val, b_val])

"""Plotting the data"""
plt.plot(loss_values, 'ro')
plt.show()
cr, cg, cb = (1.0, 1.0, 0.0)
for f in train_data:
    cb += 1.0 / len(train_data)
    cg -= 1.0 / len(train_data)
    if cb > 1.0: cb = 1.0
    if cg < 0.0: cg = 0.0
    [a, b] = f
    f_y = np.vectorize(lambda x: a*x + b)(train_x)
    line = plt.plot(train_x, f_y)
    plt.setp(line, color=(cr,cg,cb))

plt.plot(train_x, train_y, 'ro')

green_line = mpatches.Patch(color='red', label='Data Points')

plt.legend(handles=[green_line])

plt.show()