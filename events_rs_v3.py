import tensorflow.compat.v1 as tf
# Numpy contains helpful functions for efficient mathematical calculations
import numpy as np
# Dataframe manipulation library
import pandas as pd
# Graph plotting library
import matplotlib.pyplot as plt

pd.options.display.width = 0
tf.disable_v2_behavior()

"""GETTING ALL THE INPUT DATAS"""
ratings_df = pd.read_csv('data/data_csv/ue_participations.csv', sep=':', header=None, engine='python')
events_df = pd.read_csv('data/data_csv/events.csv', sep=':', header=None, engine='python')
users_df = pd.read_csv('data/data_csv/users.csv', sep=':', header=None, engine='python')
ratings_df.columns = ['UserID', 'EventID', 'Participation']
events_df.columns = ['EventID', 'OrganizerID', 'OfficeTarget','Categories']
users_df.columns = ['UserID', 'Office', 'Chapter', 'Interests']
# print(ratings_df.head())

user_participation_df = ratings_df.pivot(index='UserID', columns='EventID', values='Participation')
print(user_participation_df.head())

trX = user_participation_df.values
index = user_participation_df.index
print(trX)

"""SETTING UP RBM PARAMETERS:
Number of hidden units: hiddenUnits
Number of visible units: visibleUnits
Bias for visible units: vb
Bias for hidden units: hb
Weight matrix: W
"""
hiddenUnits = 30 #Number of features we're going to learn
visibleUnits = len(user_participation_df.columns) # 400 Number of unique events
vb = tf.placeholder("float", [visibleUnits]) # visible bias vector
hb = tf.placeholder("float", [hiddenUnits]) # hidden bias vector
W = tf.placeholder("float", [visibleUnits, hiddenUnits]) # weight matrix visib x hidd


"""
Building the graph with the training operations
"""
"""GIBBS SAMPLING FOR CD-2"""
#Phase 1: Input Processing
v0 = tf.placeholder("float", [None, visibleUnits])
_h0 = tf.nn.sigmoid(tf.matmul(v0, W) + hb)  # calculating probabilities of turning hidden units on
h0 = tf.nn.relu(tf.sign(_h0 - tf.random_uniform(tf.shape(_h0)))) # drawing a sample from the distribution
#Phase 2: Reconstruction
_v1 = tf.nn.sigmoid(tf.matmul(h0, tf.transpose(W)) + vb) # calculating probabilities of turning visible units on
v1 = tf.nn.relu(tf.sign(_v1 - tf.random_uniform(tf.shape(_v1)))) # sampling from visible units distribution
h1 = tf.nn.sigmoid(tf.matmul(v1, W) + hb) # correspondent hidden units

# one more step for CD-2
_v2 = tf.nn.sigmoid(tf.matmul(h1, tf.transpose(W)) + vb)
v2 = tf.nn.relu(tf.sign(_v2 - tf.random_uniform(tf.shape(_v2)))) # sampling from visible units distribution
h2 = tf.nn.sigmoid(tf.matmul(v2, W) + hb)

"""CONTRASTIVE DIVERGENCE"""
#Learning rate
alpha = 1.0
#Create the gradients
w_pos_grad = tf.matmul(tf.transpose(v0), h0)
# w_neg_grad = tf.matmul(tf.transpose(v1), h1)
w_neg_grad = tf.matmul(tf.transpose(v2), h2)
#Calculate the Contrastive Divergence to maximize
CD = (w_pos_grad - w_neg_grad) / tf.to_float(tf.shape(v0)[0])
#Create methods to update the weights and biases
update_w = W + alpha * CD
# update_vb = vb + alpha * tf.reduce_mean(v0 - v1, 0)
update_vb = vb + alpha * tf.reduce_mean(v0 - v2, 0)
update_hb = hb + alpha * tf.reduce_mean(h0 - h2, 0)
# update_hb = hb + alpha * tf.reduce_mean(h0 - h1, 0)

err = v0 - v2
# err = v0 - v1
err_sum = tf.reduce_mean(err * err)

"""Variable initialisations"""
#Current weight
cur_w = np.zeros([visibleUnits, hiddenUnits], np.float32)
#Current visible unit biases
cur_vb = np.zeros([visibleUnits], np.float32)
#Current hidden unit biases
cur_hb = np.zeros([hiddenUnits], np.float32)
#Previous weight
prv_w = np.zeros([visibleUnits, hiddenUnits], np.float32)
#Previous visible unit biases
prv_vb = np.zeros([visibleUnits], np.float32)
#Previous hidden unit biases
prv_hb = np.zeros([hiddenUnits], np.float32)
sess = tf.Session()
sess.run(tf.global_variables_initializer())

epochs = 1000
batchsize = 50
errors = []
for i in range(epochs):
    for start, end in zip( range(0, len(trX), batchsize), range(batchsize, len(trX), batchsize)):
        batch = trX[start:end]
        # print(start, end)
        # print(batch.shape, prv_hb.shape)
        cur_w = sess.run(update_w, feed_dict={v0: batch, W: prv_w, vb: prv_vb, hb: prv_hb})
        cur_vb = sess.run(update_vb, feed_dict={v0: batch, W: prv_w, vb: prv_vb, hb: prv_hb})
        cur_hb = sess.run(update_hb, feed_dict={v0: batch, W: prv_w, vb: prv_vb, hb: prv_hb})
        prv_w = cur_w
        prv_vb = cur_vb
        prv_hb = cur_hb
    errors.append(sess.run(err_sum, feed_dict={v0: trX, W: cur_w, vb: cur_vb, hb: cur_hb}))
    print (errors[-1])
plt.plot(errors)
plt.ylabel('Error')
plt.xlabel('Epoch')
plt.show()

"""TRAINING ENDED"""

"""INFEERNCE TIME:)"""
#Selecting the input user
inputUser = trX[0].reshape(1, -1)
print(inputUser)

"""Graph for running inference"""
#Feeding in the user and reconstructing the input
hh0 = tf.nn.sigmoid(tf.matmul(v0, W) + hb)
vv1 = tf.nn.sigmoid(tf.matmul(hh0, tf.transpose(W)) + vb)
feed = sess.run(hh0, feed_dict={v0: inputUser, W: prv_w, hb: prv_hb})
rec = sess.run(vv1, feed_dict={hh0: feed, W: prv_w, vb: prv_vb})
# print(rec)

scored_events_df_mock = events_df[events_df['EventID'].isin(user_participation_df.columns)]
scored_events_df_mock = scored_events_df_mock.assign(RecommendationScore=rec[0])
scored_events_df_mock = scored_events_df_mock[scored_events_df_mock['EventID']>299]
# print(scored_events_df_mock.sort_values(["RecommendationScore"], ascending=False).head(20))
# with open('recommendation_score_tf_dl_v1.txt', 'w') as reco:
#     reco.write(scored_events_df_mock.sort_values(["RecommendationScore"], ascending=False).to_string())

sel_user_participations_df = ratings_df[ratings_df['UserID'] == 0]
# print(sel_user_participations_df.to_string())
# sel_user_non_part_events_df = sel_user_participations_df[sel_user_participations_df['Participation'] == 0]

merged_df = pd.merge(sel_user_participations_df, scored_events_df_mock, how='inner', on='EventID')
# merged_df = pd.merge(sel_user_non_part_events_df, scored_events_df_mock, how='inner', on='EventID')
final_df = merged_df.sort_values(['RecommendationScore'], ascending=False).head(20)
with open('recommendation_score_tf_dl_v3.txt', 'w') as reco:
    reco.write(final_df.to_string())
print(final_df.head(20))
