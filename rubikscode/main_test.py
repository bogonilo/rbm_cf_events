from rbm import RBM
import numpy as np
import pandas as pd
import tensorflow.compat.v1 as tf

pd.options.display.width = 0
tf.disable_v2_behavior()

ratings_df = pd.read_csv('../data/data_csv/ue_participations.csv', sep=':', header=None, engine='python')
events_df = pd.read_csv('../data/data_csv/events.csv', sep=':', header=None, engine='python')
users_df = pd.read_csv('../data/data_csv/users.csv', sep=':', header=None, engine='python')
ratings_df.columns = ['UserID', 'EventID', 'Participation']
events_df.columns = ['EventID', 'OrganizerID', 'OfficeTarget','Categories']
users_df.columns = ['UserID', 'Office', 'Chapter', 'Interests']
# print(ratings_df.head())

user_partecipation_df = ratings_df.pivot(index='UserID', columns='EventID', values='Participation')
# print(user_partecipation_df.head())

trX = user_partecipation_df.values
index = user_partecipation_df.index
print(trX)
# test = np.array([[0,1,1,0], [0,1,0,0], [0,0,1,1]])
rbm = RBM(400, 40, 0.1, 100)
rbm.train(trX)
# rbm.train(test)chapter_match+location_match<=1
#Selecting the input user
# print(index[35])
# print(trX[35])
inputUser = trX[0].reshape(1, -1)
inputUser = inputUser.astype(np.float32)
# print(inputUser)
selected_user = users_df[users_df['UserID'] == 0]
# print(selected_user.iloc[0]['Office'])
reconstructed = rbm.reconstruct(inputUser)

print('**** Getting 20 recommended events ****')
scored_events_df_mock = events_df[events_df['EventID'].isin(user_partecipation_df.columns)]
scored_events_df_mock = scored_events_df_mock.assign(RecommendationScore=reconstructed[0])
# scored_events_df_mock = scored_events_df_mock[scored_events_df_mock['OfficeTarget'] == selected_user.iloc[0]['Office']]
# print(scored_events_df_mock.sort_values(['RecommendationScore'], ascending=False).head(20))
with open('recommendation_score_rubiks.txt', 'w') as reco:
    reco.write(scored_events_df_mock.sort_values(["RecommendationScore"], ascending=False).to_string())