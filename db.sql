CREATE TABLE user(
id VARCHAR(40) PRIMARY KEY,
office VARCHAR(40) NOT NULL,
chapter VARCHAR(40) NOT NULL,
interests VARCHAR(150) NOT NULL);

CREATE TABLE event(
id VARCHAR(40) PRIMARY KEY,
userId VARCHAR(40) NOT NULL,
target VARCHAR(40) NOT NULL,
categories VARCHAR(150) NOT NULL,
FOREIGN KEY (userId) REFERENCES user(id)
);

CREATE TABLE participation(
userId VARCHAR(40) NOT NULL,
eventId VARCHAR(40) NOT NULL,
participation SMALLINT NOT NULL,
PRIMARY KEY (userId, eventId),
FOREIGN KEY (userId) REFERENCES user(id),
FOREIGN KEY (eventId) REFERENCES event(id)
);

CREATE TABLE recommendation(
userId VARCHAR(40) NOT NULL,
eventId VARCHAR(40) NOT NULL,
recommendation FLOAT(10) NOT NULL,
PRIMARY KEY (userId, eventId),
FOREIGN KEY (userId) REFERENCES user(id),
FOREIGN KEY (eventId) REFERENCES event(id)
);

INSERT INTO user VALUES ('1', 'Amsterdam (ITO, Building 01)', 'Android Development', 'Food & Drink,Outside Activity,Sport & Wellness')
UPDATE participation SET participation = 1 WHERE userId = '1' AND eventId = 'WtZi41EGncOlPbniKil3';
UPDATE participation SET participation = 1 WHERE userId = '1' AND eventId = '1tBlfkh9wDA9qQYMtOZr';
UPDATE participation SET participation = 1 WHERE userId = '1' AND eventId = '84ody7tYN4lzi2JlEfgh';
UPDATE participation SET participation = 1 WHERE userId = '1' AND eventId = 'TUmRmRLKRd7bdJXyoWvt';
UPDATE participation SET participation = 1 WHERE userId = '1' AND eventId = 'KZchp4aGzVbo4l56uk8M';
UPDATE participation SET participation = 0 WHERE userId = '1' AND eventId = 'WNG8I5nIQzuTrhZB8g92';
UPDATE participation SET participation = 0 WHERE userId = '1' AND eventId = 'izzC3fBA0VLIQAQJtwNU';
UPDATE participation SET participation = 0 WHERE userId = '1' AND eventId = 'RIOmKqrr3PDs6qSjZC6R';
UPDATE participation SET participation = 0 WHERE userId = '1' AND eventId = 'mHbgmYccwSKUTIP2OdE6';
UPDATE participation SET participation = 0 WHERE userId = '1' AND eventId = '6y7r6ChGtxIqaeQ8LS46';

UPDATE participation SET participation = 1 WHERE userId = '2' AND eventId = 'WtZi41EGncOlPbniKil3';
UPDATE participation SET participation = 0 WHERE userId = '2' AND eventId = '1tBlfkh9wDA9qQYMtOZr';
UPDATE participation SET participation = 0 WHERE userId = '2' AND eventId = '84ody7tYN4lzi2JlEfgh';
UPDATE participation SET participation = 1 WHERE userId = '2' AND eventId = 'TUmRmRLKRd7bdJXyoWvt';
UPDATE participation SET participation = 1 WHERE userId = '2' AND eventId = 'KZchp4aGzVbo4l56uk8M';
UPDATE participation SET participation = 1 WHERE userId = '2' AND eventId = 'WNG8I5nIQzuTrhZB8g92';
UPDATE participation SET participation = 1 WHERE userId = '2' AND eventId = 'izzC3fBA0VLIQAQJtwNU';
UPDATE participation SET participation = 0 WHERE userId = '2' AND eventId = 'RIOmKqrr3PDs6qSjZC6R';
UPDATE participation SET participation = 0 WHERE userId = '2' AND eventId = 'mHbgmYccwSKUTIP2OdE6';
UPDATE participation SET participation = 0 WHERE userId = '2' AND eventId = '6y7r6ChGtxIqaeQ8LS46';

UPDATE participation SET participation = 1 WHERE userId = '3' AND eventId = 'WtZi41EGncOlPbniKil3';
UPDATE participation SET participation = 0 WHERE userId = '3' AND eventId = '1tBlfkh9wDA9qQYMtOZr';
UPDATE participation SET participation = 0 WHERE userId = '3' AND eventId = '84ody7tYN4lzi2JlEfgh';
UPDATE participation SET participation = 1 WHERE userId = '3' AND eventId = 'TUmRmRLKRd7bdJXyoWvt';
UPDATE participation SET participation = 1 WHERE userId = '3' AND eventId = 'KZchp4aGzVbo4l56uk8M';
UPDATE participation SET participation = 0 WHERE userId = '3' AND eventId = 'WNG8I5nIQzuTrhZB8g92';
UPDATE participation SET participation = 1 WHERE userId = '3' AND eventId = 'izzC3fBA0VLIQAQJtwNU';
UPDATE participation SET participation = 0 WHERE userId = '3' AND eventId = 'RIOmKqrr3PDs6qSjZC6R';
UPDATE participation SET participation = 0 WHERE userId = '3' AND eventId = 'mHbgmYccwSKUTIP2OdE6';
UPDATE participation SET participation = 0 WHERE userId = '3' AND eventId = '6y7r6ChGtxIqaeQ8LS46';

UPDATE participation SET participation = 1 WHERE userId = '4' AND eventId = 'WtZi41EGncOlPbniKil3';
UPDATE participation SET participation = 1 WHERE userId = '4' AND eventId = '1tBlfkh9wDA9qQYMtOZr';
UPDATE participation SET participation = 1 WHERE userId = '4' AND eventId = '84ody7tYN4lzi2JlEfgh';
UPDATE participation SET participation = 1 WHERE userId = '4' AND eventId = 'TUmRmRLKRd7bdJXyoWvt';
UPDATE participation SET participation = 0 WHERE userId = '4' AND eventId = 'KZchp4aGzVbo4l56uk8M';
UPDATE participation SET participation = 0 WHERE userId = '4' AND eventId = 'WNG8I5nIQzuTrhZB8g92';
UPDATE participation SET participation = 0 WHERE userId = '4' AND eventId = 'izzC3fBA0VLIQAQJtwNU';
UPDATE participation SET participation = 0 WHERE userId = '4' AND eventId = 'RIOmKqrr3PDs6qSjZC6R';
UPDATE participation SET participation = 0 WHERE userId = '4' AND eventId = 'mHbgmYccwSKUTIP2OdE6';
UPDATE participation SET participation = 0 WHERE userId = '4' AND eventId = '6y7r6ChGtxIqaeQ8LS46';

UPDATE participation SET participation = 0 WHERE userId = '5' AND eventId = 'WtZi41EGncOlPbniKil3';
UPDATE participation SET participation = 0 WHERE userId = '5' AND eventId = '1tBlfkh9wDA9qQYMtOZr';
UPDATE participation SET participation = 0 WHERE userId = '5' AND eventId = '84ody7tYN4lzi2JlEfgh';
UPDATE participation SET participation = 0 WHERE userId = '5' AND eventId = 'TUmRmRLKRd7bdJXyoWvt';
UPDATE participation SET participation = 0 WHERE userId = '5' AND eventId = 'KZchp4aGzVbo4l56uk8M';
UPDATE participation SET participation = 0 WHERE userId = '5' AND eventId = 'WNG8I5nIQzuTrhZB8g92';
UPDATE participation SET participation = 0 WHERE userId = '5' AND eventId = 'izzC3fBA0VLIQAQJtwNU';
UPDATE participation SET participation = 0 WHERE userId = '5' AND eventId = 'RIOmKqrr3PDs6qSjZC6R';
UPDATE participation SET participation = 1 WHERE userId = '5' AND eventId = 'mHbgmYccwSKUTIP2OdE6';
UPDATE participation SET participation = 1 WHERE userId = '5' AND eventId = '6y7r6ChGtxIqaeQ8LS46';

UPDATE participation SET participation = 0 WHERE userId = '6' AND eventId = 'WtZi41EGncOlPbniKil3';
UPDATE participation SET participation = 0 WHERE userId = '6' AND eventId = '1tBlfkh9wDA9qQYMtOZr';
UPDATE participation SET participation = 0 WHERE userId = '6' AND eventId = '84ody7tYN4lzi2JlEfgh';
UPDATE participation SET participation = 0 WHERE userId = '6' AND eventId = 'TUmRmRLKRd7bdJXyoWvt';
UPDATE participation SET participation = 0 WHERE userId = '6' AND eventId = 'KZchp4aGzVbo4l56uk8M';
UPDATE participation SET participation = 0 WHERE userId = '6' AND eventId = 'WNG8I5nIQzuTrhZB8g92';
UPDATE participation SET participation = 0 WHERE userId = '6' AND eventId = 'izzC3fBA0VLIQAQJtwNU';
UPDATE participation SET participation = 1 WHERE userId = '6' AND eventId = 'RIOmKqrr3PDs6qSjZC6R';
UPDATE participation SET participation = 1 WHERE userId = '6' AND eventId = 'mHbgmYccwSKUTIP2OdE6';
UPDATE participation SET participation = 1 WHERE userId = '6' AND eventId = '6y7r6ChGtxIqaeQ8LS46';

UPDATE participation SET participation = 0 WHERE userId = 'QHImzDTt2hUZGNzqcj4O4ipCtUj1' AND eventId = 'WtZi41EGncOlPbniKil3';
UPDATE participation SET participation = 0 WHERE userId = 'QHImzDTt2hUZGNzqcj4O4ipCtUj1' AND eventId = '1tBlfkh9wDA9qQYMtOZr';
UPDATE participation SET participation = 0 WHERE userId = 'QHImzDTt2hUZGNzqcj4O4ipCtUj1' AND eventId = '84ody7tYN4lzi2JlEfgh';
UPDATE participation SET participation = 0 WHERE userId = 'QHImzDTt2hUZGNzqcj4O4ipCtUj1' AND eventId = 'TUmRmRLKRd7bdJXyoWvt';
UPDATE participation SET participation = 0 WHERE userId = 'QHImzDTt2hUZGNzqcj4O4ipCtUj1' AND eventId = 'KZchp4aGzVbo4l56uk8M';
UPDATE participation SET participation = 0 WHERE userId = 'QHImzDTt2hUZGNzqcj4O4ipCtUj1' AND eventId = 'WNG8I5nIQzuTrhZB8g92';
UPDATE participation SET participation = 0 WHERE userId = 'QHImzDTt2hUZGNzqcj4O4ipCtUj1' AND eventId = 'izzC3fBA0VLIQAQJtwNU';
UPDATE participation SET participation = 0 WHERE userId = 'QHImzDTt2hUZGNzqcj4O4ipCtUj1' AND eventId = 'RIOmKqrr3PDs6qSjZC6R';
UPDATE participation SET participation = 0 WHERE userId = 'QHImzDTt2hUZGNzqcj4O4ipCtUj1' AND eventId = 'mHbgmYccwSKUTIP2OdE6';
UPDATE participation SET participation = 0 WHERE userId = 'QHImzDTt2hUZGNzqcj4O4ipCtUj1' AND eventId = '6y7r6ChGtxIqaeQ8LS46';

UPDATE recommendation SET recommendation="0.00002223" WHERE userId='QHImzDTt2hUZGNzqcj4O4ipCtUj1' AND eventId='1tBlfkh9wDA9qQYMtOZr'

SELECT count(userId) FROM participation WHERE participation=1 AND eventId='WtZi41EGncOlPbniKil3';
SELECT count(*) FROM user;