import tensorflow.compat.v1 as tf
import pandas as pd

tf.disable_v2_behavior()

"""GETTING ALL THE INPUT DATAS"""
ratings_df = pd.read_csv('data/data_csv/ue_participations.csv', sep=':', header=None, engine='python')
events_df = pd.read_csv('data/data_csv/events.csv', sep=':', header=None, engine='python')
users_df = pd.read_csv('data/data_csv/users.csv', sep=':', header=None, engine='python')
ratings_df.columns = ['UserID', 'EventID', 'Participation']
events_df.columns = ['EventID', 'OrganizerID', 'OfficeTarget','Categories']
users_df.columns = ['UserID', 'Office', 'Chapter', 'Interests']
# print(ratings_df.head())

user_participation_df = ratings_df.pivot(index='UserID', columns='EventID', values='Participation')

trX = user_participation_df.values
index = user_participation_df.index

"""INFEERNCE TIME:)"""
#Selecting the input user
inputUser = trX[0].reshape(1, -1)

saver = tf.train.import_meta_graph('save_model/save_model.meta')

# list all the tensors in the graph
# for tensor in tf.get_default_graph().get_operations():
#     print(tensor.name)

sess = tf.Session()
saver.restore(sess,'save_model/save_model')

weight = sess.run(['weight:0'])
print('W = ', weight)

# """Graph for running inference"""
#
# feed = sess.run("hh0", feed_dict={"v0:0": inputUser})
# rec = sess.run("vv1", feed_dict={"hh0:0": feed})
# # print(rec)
#
# scored_events_df_mock = events_df[events_df['EventID'].isin(user_participation_df.columns)]
# scored_events_df_mock = scored_events_df_mock.assign(RecommendationScore=rec[0])
# scored_events_df_mock = scored_events_df_mock[scored_events_df_mock['EventID']>299]
# # print(scored_events_df_mock.sort_values(["RecommendationScore"], ascending=False).head(20))
# # with open('recommendation_score_tf_dl_v1.txt', 'w') as reco:
# #     reco.write(scored_events_df_mock.sort_values(["RecommendationScore"], ascending=False).to_string())
#
# sel_user_participations_df = ratings_df[ratings_df['UserID'] == 0]
# # print(sel_user_participations_df.to_string())
# # sel_user_non_part_events_df = sel_user_participations_df[sel_user_participations_df['Participation'] == 0]
#
# merged_df = pd.merge(sel_user_participations_df, scored_events_df_mock, how='inner', on='EventID')
# # merged_df = pd.merge(sel_user_non_part_events_df, scored_events_df_mock, how='inner', on='EventID')
# final_df = merged_df.sort_values(['RecommendationScore'], ascending=False).head(20)
# with open('recommendation_score_tf_dl_v1.txt', 'w') as reco:
#     reco.write(final_df.to_string())
# print(final_df.head(20))
