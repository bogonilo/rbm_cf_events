import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore

# Use a service account
cred = credentials.Certificate('../credentials/connective-cp-firebase-adminsdk-22237-1bf8e3cb67.json')
firebase_admin.initialize_app(cred)

db = firestore.client()


# Create a callback on_snapshot function to capture changes
def on_snapshot_callback(doc_snapshot, changes, read_time):
    for doc in doc_snapshot:
        print(u'Received document snapshot: {}'.format(doc.id))


doc_ref = db.collection('users').document('JE5BToKfzN1kJ4LsnNin')

# Watch the document
doc_watch = doc_ref.on_snapshot(on_snapshot_callback)
