import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore
from models.user import User
from models.event import Event
import datetime

# Use a service account
cred = credentials.Certificate('/home/lorenzo/Desktop/Collegamento a Università/Magistrale/Thesis/rbm_cf_events/credentials/connective-cp-firebase-adminsdk-22237-1bf8e3cb67.json')
firebase_admin.initialize_app(cred)

db = firestore.client()


# class User:
#     def __init__(self, chapter, email, interests, joinedIdeas, name, office, position,
#                  skills, uid,
#                  profileImgUrl="https://firebasestorage.googleapis.com/v0/b/connective-cp.appspot.com/o/profile_images%2Faccenture_logo.png?alt=media&token=e50b5f71-4aaf-4d23-bfd1-5d6c8a76d3c9"):

# user_1 = User('Android Development','email1@accenture.com', ['Food & Drink', 'Outside Activity', 'Sport & Wellness'],
#               [], 'Paul McCartney', 'Amsterdam(ITO, Building 01)', 'Associate', ['skill1', 'skill2', 'skill3'], '1')
# user_2 = User('Business Analyst/Developer','email2@accenture.com', ['Party','Music','Volunteering'], [], 'John Lennon',
#               'Amsterdam(ITO, Building 01)', 'Associate', ['skill1', 'skill2', 'skill3'], '2')
# user_3 = User('Business Analyst/Developer','email3@accenture.com', ['Food & Drink','Music','Networking'], [],
#               'Keith Richards', 'Amsterdam(ITO, Building 01)', 'Associate', ['skill1', 'skill2', 'skill3'], '3')
# user_4 = User('iOS Development','email4@accenture.com', ['Party','Food & Drink','Culture & Art'], [], 'Mick Jagger',
#               'Amsterdam(ITO, Building 01)', 'Associate', ['skill1', 'skill2', 'skill3'], '4')
# user_5 = User('Business Analyst/Developer','email5@accenture.com', ['Travel','Food & Drink','Culture & Art'], [],
#               'David Bowie', 'Malaga', 'Associate', ['skill1', 'skill2', 'skill3'], '5')
# user_6 = User('Project Manager','email6@accenture.com', ['Travel','Game','Career & Business'], [], 'Freddie Mercury',
#               'Malaga', 'Associate', ['skill1', 'skill2', 'skill3'], '6')
#
# ref = db.collection('users')
# ref.add(user_1.get_data())
# ref.add(user_2.get_data())
# ref.add(user_3.get_data())
# ref.add(user_4.get_data())
# ref.add(user_5.get_data())
# ref.add(user_6.get_data())

# class Event:
#     def __init__(self, categories, creatorName, creatorUid, description, id,
#                  joiners, location, startDate, target, title):

# event_0 = Event(['Food & Drink'], 'Paul McCartney', '1', 'The knowledge of territories and their peculiarities, passes'
#                                                        ' through the people who, in these spaces, dedicate passion, time'
#                                                        ' and effort, to preserve and improve them.', 'id', [{'uid':'1'}], 'Vondelpark',
#                 datetime.datetime(2020, 4, 12, 12, 00), ['Amsterdam (ITO, Building 01)'], 'Wine Tasting Experience')
event_1 = Event(['Food & Drink', 'Outside Activity', 'Party'], 'Paul McCartney', '1', 'I\'m organizing a spring barbecue'
                                                                                      ' party to celebrate Spring. BYOB',
                'id', [{'uid':'1'}], 'My house',
                datetime.datetime(2020, 3, 21, 16, 00), ['Amsterdam (ITO, Building 01)'], 'Spring Barbecue Party')
event_2 = Event(['Outside Activity', 'Sport & Wellness'], 'Paul McCartney', '1', 'Let\'s meet up for a jog together. '
                                                                                 'Den Haague is going to be perfect in that period',
                'id', [{'uid':'1'}], 'The Hague',
                datetime.datetime(2020, 4, 24, 10, 00), ['Amsterdam (ITO, Building 01)'], 'Jog in Den Haague')
event_3 = Event(['Food & Drink'], 'Paul McCartney', '1', 'End of the quarter team dinner', 'id', [{'uid':'1'}], 'Genki Sushi',
                datetime.datetime(2020, 3, 31, 18, 30), ['Amsterdam (ITO, Building 01)'], 'Team Dinner')
event_4 = Event(['Party', 'Music'], 'John Lennon', '2', 'The official European cover band of The Beatles is coming to Amsterdam. '
                                          'Let\'s go have some fun!', 'id', [{'uid':'2'}], 'Ziggo Dome',
                datetime.datetime(2020, 5, 25, 19, 30), ['Amsterdam (ITO, Building 01)'], 'Team Dinner')
event_5 = Event(['Music'], 'John Lennon', '2', 'We have booked a practice studio for the Saturday afternoon, if you know how to '
                                               'play an instrument(or not:)) you are very welcome to join.',
                'id', [{'uid':'2'}], 'Music Studio',
                datetime.datetime(2020, 3, 20, 13, 30), ['Amsterdam (ITO, Building 01)'], 'Free Jam Session')
event_6 = Event(['Volunteering', 'Music'], 'John Lennon', '2', 'The Anonym Band is going to perform for charity'
                                                               'in Dam square this Sunday. With a free offer you can '
                                                               'contribute to some good causes', 'id', [{'uid':'2'}], 'Dam Square',
                datetime.datetime(2020, 3, 31, 18, 30), ['Amsterdam (ITO, Building 01)'], 'Charity Event')
event_7 = Event(['Travel', 'Outside Activity'], 'Freddie Mercury', '6', 'This May we are organizing a trip to Amsterdam.'
                                                                        'In occasion of the new system we want to go say hi'
                                                                        'to our northern colleagues', 'id',
                [{'uid':'6'}], 'Malaga Office', datetime.datetime(2020, 5, 1, 9, 30), ['Malaga'], 'Netherlands Trip')
event_8 = Event(['Game'], 'Freddie Mercury', '6', 'Annual Fifa Tournament. Winner takes all. 5€ subscription',
                'id', [{'uid':'6'}], 'Office Break Room',
                datetime.datetime(2020, 4, 1, 14, 30), ['Malaga'], 'Fifa Tournament')
event_9 = Event(['Career & Business'], 'Freddie Mercury', '6', 'Due to the new updates introduced to the Atlassian product'
                                                               'Jira, I\'ve prepared a short presentation with '
                                                               'the new functionalities.', 'id', [{'uid':'6'}],
                'Conference Room', datetime.datetime(2020, 3, 10, 13, 30), ['Malaga'], 'Jira Workflow workshop')

ref = db.collection('ideas')
ref.add(event_1.get_data())
ref.add(event_2.get_data())
ref.add(event_3.get_data())
ref.add(event_4.get_data())
ref.add(event_5.get_data())
ref.add(event_6.get_data())
ref.add(event_7.get_data())
ref.add(event_8.get_data())
ref.add(event_9.get_data())

