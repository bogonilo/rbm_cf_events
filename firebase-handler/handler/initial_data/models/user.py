import csv

class User:
    def __init__(self, chapter, email, interests, joinedIdeas, name, office, position,
                 skills, uid, profileImgUrl="https://firebasestorage.googleapis.com/v0/b/connective-cp.appspot.com/o/profile_images%2Faccenture_logo.png?alt=media&token=e50b5f71-4aaf-4d23-bfd1-5d6c8a76d3c9"):
        self.chapter = chapter
        self.email = email
        self.interests = interests
        self.joinedIdeas = joinedIdeas
        self.name = name
        self.office = office
        self.position = position
        self.skills = skills
        self.uid = uid
        self.profileImgUrl = profileImgUrl

    def get_data(self):
        data_user = {
            'bio': "",
            'chapter': self.chapter,
            'email': self.email,
            'interests': self.interests,
            'joinedIdeas': self.joinedIdeas,
            'name': self.name,
            'office': self.office,
            'phoneN': '',
            'position': self.position,
            'profileImgUrl': self.profileImgUrl,
            'skills': self.skills,
            'uid': self.uid
        }
        return data_user

    @staticmethod
    def create_user_from_doc(doc):
        return User(doc.get('chapter'),doc.get('email'),doc.get('interests'),doc.get('joinedIdeas'),doc.get('name'),
                    doc.get('office'),doc.get('position'),doc.get('skills'),doc.get('uid'))

    def add_user_to_csv(self):
        with open('/home/lorenzo/Desktop/Collegamento a Università/Magistrale/Thesis/rbm_cf_events/firebase-handler/handler/csv/users.csv', mode='a') as users_csv:
            writer = csv.writer(users_csv, delimiter=':', quotechar='"')
            office = self.office
            chapter = self.chapter
            interest = self.interests
            writer.writerow([str(self.uid), str(office), str(chapter), str(interest[0] + ',' + interest[1] +
                                                                       ',' + interest[2])])
            users_csv.close()

    def add_user_to_db(self, mydb):
        cursor = mydb.cursor()
        sql_clause = "INSERT IGNORE INTO user VALUES ('"+self.uid+"', '"+self.office+"', '"+self.chapter+"', '"+\
                     self.interests[0]+","+self.interests[1]+","+self.interests[2]+"')"
        cursor.execute(sql_clause)
        cursor.close()
        mydb.commit()
        mydb.close()

    def calculate_score(self, id, organizer, target, categories, event_joiners, users_number):
        # print(organizer['id'], organizer['office'], organizer['chapter'], organizer['interests'])
        popularity_value = event_joiners / users_number
        if (organizer['office'] == self.office):
            organizer_location_match = 1
        else:
            organizer_location_match = 0
        if (organizer['chapter'] == self.chapter):
            chapter_match = 1
        else:
            chapter_match = 0
        if (target == self.office):
            target_match = 1
        else:
            target_match = 0
        matching_interests = set(categories.split(',')) & set(self.interests)
        if (len(matching_interests) > 0):
            match_interests = 1
        else:
            match_interests = 0
        return (organizer_location_match + chapter_match + target_match + match_interests)*0.4 + popularity_value*0.6

    def calculate_relationships(self, mydb):
        cursor_events = mydb.cursor(buffered=True)
        cursor_organizer = mydb.cursor(dictionary=True)
        cursor_recommendation = mydb.cursor()
        cursor_participation = mydb.cursor()
        sql_clause_events = "SELECT id, userId, target, categories FROM event"
        query_organizer = ("SELECT id, office, chapter, interests FROM user WHERE id=")
        query_insert_recommendation_score = "INSERT IGNORE INTO recommendation VALUES "
        query_insert_participation = "INSERT IGNORE INTO participation VALUES "
        cursor_events.execute(sql_clause_events)
        for (id, userId, target, categories) in cursor_events:
            cursor_organizer.execute(query_organizer+"'"+userId+"'")
            organizer = cursor_organizer.fetchone()
            reccomendation_score = self.calculate_score(id, organizer, target, categories)
            val_r = "('"+self.uid+"', '"+id+"', "+str(reccomendation_score)+")"
            cursor_recommendation.execute(query_insert_recommendation_score+val_r)
            val_p = "('"+self.uid+"', '"+id+"', 0)"
            cursor_participation.execute(query_insert_participation+val_p)
        cursor_organizer.close()
        cursor_recommendation.close()
        cursor_events.close()
        cursor_participation.close()
        mydb.commit()
        mydb.close()

