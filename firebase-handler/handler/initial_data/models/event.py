import datetime
import csv


class Event:
    def __init__(self, categories, creatorName, creatorUid, description, id,
                 joiners, location, startDate, target, title):
        self.categories = categories
        self.creatorName = creatorName
        self.creatorUid = creatorUid
        self.description = description
        self.id = id
        self.joiners = joiners
        self.location = location
        self.startDate = startDate
        self.target = target[0]
        self.title = title
        self.str_cat = self.categories[0]
        if (len(self.categories) > 1):
            for item in self.categories[1:]:
                self.str_cat = self.str_cat + ',' + item

    def get_data(self):
        temp_date = datetime.datetime.now()
        data_event = {
            'categories': self.categories,
            'creatorName': self.creatorName,
            'creatorUid': self.creatorUid,
            'description': self.description,
            'endDate': temp_date,
            'endDateSet': False,
            'id': self.id,
            'joiners': self.joiners,
            'location': self.location,
            'maxPartecipants':100,
            'minPartecipants':2,
            'pictureUrl': "",
            'rsvpDate': temp_date,
            'rsvpDateSet': False,
            'startDate': self.startDate,
            'target': self.target,
            'title': self.title,
            'updates': []
        }
        return data_event

    @staticmethod
    def create_event_from_doc(doc):
        return Event(doc.get('categories'), doc.get('creatorName'), doc.get('creatorUid'), doc.get('description'),
                     doc.id, doc.get('joiners'), doc.get('location'), doc.get('startDate'), doc.get('target'),
                     doc.get('title'))

    def add_event_to_csv(self):
        with open('/home/lorenzo/Desktop/Collegamento a Università/Magistrale/Thesis/rbm_cf_events/firebase-handler'
                  '/handler/csv/events.csv', mode='a') as events_csv:
            writer = csv.writer(events_csv, delimiter=':', quotechar='"')
            id = self.id
            organizer = self.creatorUid
            office_target = self.target
            str_cat = self.str_cat
            writer.writerow([id, organizer, office_target, str_cat])
            events_csv.close()

    def add_event_to_db(self, mydb):
        cursor = mydb.cursor()
        sql_clause = "INSERT IGNORE INTO event VALUES ('"+self.id+"','"+self.creatorUid+"','"+self.target+"','"+self.str_cat+"')"
        cursor.execute(sql_clause)
        new = cursor.lastrowid
        if( new != 0 ):
            cursor_users = mydb.cursor()
            cursor_participation = mydb.cursor()
            query_users = "SELECT id FROM user"
            cursor_users.execute(query_users)
            for userId in cursor_users:
                query = "INSERT IGNORE INTO participation VALUES ('"+userId+"', '"+self.id+"', 0)"
                cursor_participation.execute(query)
            cursor_participation.close()
            cursor_users.close()
        mydb.commit()
        cursor.close()
        mydb.close()
        return new

    def calculate_score(self, user, organizer):
        if (organizer['office'] == user['office']):
            organizer_location_match = 1
        else:
            organizer_location_match = 0
        if (organizer['chapter'] == user['chapter']):
            chapter_match = 1
        else:
            chapter_match = 0
        if (self.target == user['office']):
            target_match = 1
        else:
            target_match = 0
        matching_interests = set(self.categories) & set(organizer['interests'].split(','))
        if (len(matching_interests) > 0):
            match_interests = 1
        else:
            match_interests = 0
        return (organizer_location_match + chapter_match + target_match + match_interests)/4.0

    def calculate_cold_start_values(self, mydb):
        cursor_users = mydb.cursor(dictionary=True, buffered=True)
        query_users = "SELECT id, office, chapter, interests FROM user"
        cursor_organizer = mydb.cursor(dictionary=True, buffered=True)
        cursor_recomm = mydb.cursor(buffered=True)
        cursor_users.execute(query_users)
        query_organizer = "SELECT id, office, chapter, interests FROM user WHERE id='"+self.creatorUid+"'"
        cursor_organizer.execute(query_organizer)
        organizer = cursor_organizer.fetchall()
        for user in cursor_users:
            recomm_val = self.calculate_score(user, organizer[0])
            query_recomm = "INSERT IGNORE INTO recommendation VALUES ('"+user["id"]+"', '"+self.id+"', "+str(recomm_val)+")"
            cursor_recomm.execute(query_recomm)
        mydb.commit()
        cursor_recomm.close()
        cursor_organizer.close()
        cursor_users.close()
        mydb.close()

    def remove_from_db(self, mydb):
        cursor = mydb.cursor()
        cursor_recc = mydb.cursor()
        cursor_part = mydb.cursor()
        query_recc = "DELETE FROM recommendation WHERE eventId='"+self.id+"'"
        query_part = "DELETE FROM participation WHERE eventId='"+self.id+"'"
        query = "DELETE FROM event WHERE id='"+self.id+"'"
        cursor_recc.execute(query_recc)
        cursor_part.execute(query_part)
        cursor.execute(query)
        mydb.commit()
        cursor.close()
        mydb.close()