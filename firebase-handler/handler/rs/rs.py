# from rbm import RBM
from .rbm import RBM
import mysql.connector as mysqlc
import numpy as np

def connect_to_db():
    return mysqlc.connect(
        host="localhost",
        user="root",
        passwd="qwertyuiop4!A",
        database="rbm_rs_db",
        auth_plugin='mysql_native_password'
    )

def update_rbm_recommendations(uid):
    mydb = connect_to_db()
    cursor_events = mydb.cursor(buffered=True, dictionary=True)
    cursor_users = mydb.cursor(buffered=True)
    cursor_participations = mydb.cursor(dictionary=True)
    events_n_query = "SELECT * FROM event"
    users_n_query = "SELECT id FROM user"
    participations_query = "SELECT * FROM participation"
    cursor_events.execute(events_n_query)
    event_n = cursor_events.rowcount
    cursor_users.execute(users_n_query)
    user_n = cursor_users.rowcount
    cursor_participations.execute(participations_query)
    participation_table = cursor_participations.fetchall()
    trX = np.empty(shape=(user_n, event_n))
    temp = []
    events_table = cursor_events.fetchall()
    events_ordered = []
    for i in range(0, cursor_events.rowcount):
        events_ordered.append(events_table[i]["id"])
    part_dict = {"eventsIdOrder": events_ordered}
    for i in range(1,cursor_participations.rowcount):
        temp.append(participation_table[i-1]["participation"])
        if(i % (event_n) == 0):
            # np.stack([trX, temp], axis=1)
            trX[int(i / event_n) - 1] = temp
            part_dict[participation_table[i-1]["userId"]] = temp
            # print(i, temp, participation_table[i-1]["userId"])
            temp = []
    temp.append(participation_table[cursor_participations.rowcount - 1]["participation"])
    trX[user_n-1] = temp
    part_dict[participation_table[cursor_participations.rowcount - 1]["userId"]] = temp
    rbm = RBM(event_n, 30, 1)
    rbm.train(trX)
    print("CALLED AFTER TRAINING")
    input_user = np.array(part_dict[uid])
    rec_vector = rbm.inference_recomm(input_user)
    cursor_new_recc = mydb.cursor()
    for i in range(0, len(rec_vector)):
        insert_new_recc = "UPDATE recommendation SET " \
                          "recommendation="+str(float(rec_vector[i]))+" WHERE userId='"+uid+"' AND eventId='"+part_dict["eventsIdOrder"][i]+"'"
        cursor_new_recc.execute(insert_new_recc)
        mydb.commit()
    cursor_new_recc.close()
    cursor_events.close()
    cursor_participations.close()
    cursor_users.close()

# update_rbm_recommendations(connect_to_db(), "QHImzDTt2hUZGNzqcj4O4ipCtUj1")