import tensorflow.compat.v1 as tf
#Numpy contains helpful functions for efficient mathematical calculations
import numpy as np
#Dataframe manipulation library
import pandas as pd
#Graph plotting library
import matplotlib.pyplot as plt
from rbm import RBM

pd.options.display.width = 0
tf.disable_v2_behavior()

"""GETTING ALL THE INPUT DATAS"""
ratings_df = pd.read_csv('/home/lorenzo/Desktop/Collegamento a Università/Magistrale/Thesis/rbm_cf_events/data/data_csv/ue_participations.csv', sep=':', header=None, engine='python')
events_df = pd.read_csv('/home/lorenzo/Desktop/Collegamento a Università/Magistrale/Thesis/rbm_cf_events/data/data_csv/events.csv', sep=':', header=None, engine='python')
users_df = pd.read_csv('/home/lorenzo/Desktop/Collegamento a Università/Magistrale/Thesis/rbm_cf_events/data/data_csv/users.csv', sep=':', header=None, engine='python')
ratings_df.columns = ['UserID', 'EventID', 'Participation']
events_df.columns = ['EventID', 'OrganizerID', 'OfficeTarget','Categories']
users_df.columns = ['UserID', 'Office', 'Chapter', 'Interests']
# print(ratings_df.head())

user_participation_df = ratings_df.pivot(index='UserID', columns='EventID', values='Participation')
print(user_participation_df.head())

trX = user_participation_df.values
index = user_participation_df.index
print(trX.shape)

rbm = RBM(len(user_participation_df.columns), 30, 50)
rbm.train(trX)
inputUser = trX[0].reshape(1, -1)
rec = rbm.inference_recomm(inputUser)
print(rec)

scored_events_df_mock = events_df[events_df['EventID'].isin(user_participation_df.columns)]
scored_events_df_mock = scored_events_df_mock.assign(RecommendationScore=rec[0])
scored_events_df_mock = scored_events_df_mock[scored_events_df_mock['EventID']>299]
# print(scored_events_df_mock.sort_values(["RecommendationScore"], ascending=False).head(20))
# with open('recommendation_score_tf_dl_v1.txt', 'w') as reco:
#     reco.write(scored_events_df_mock.sort_values(["RecommendationScore"], ascending=False).to_string())

sel_user_participations_df = ratings_df[ratings_df['UserID'] == 0]
# print(sel_user_participations_df.to_string())
# sel_user_non_part_events_df = sel_user_participations_df[sel_user_participations_df['Participation'] == 0]

merged_df = pd.merge(sel_user_participations_df, scored_events_df_mock, how='inner', on='EventID')
# merged_df = pd.merge(sel_user_non_part_events_df, scored_events_df_mock, how='inner', on='EventID')
final_df = merged_df.sort_values(['RecommendationScore'], ascending=False).head(20)
with open('recommendation_score_tf_dl_v1.txt', 'w') as reco:
    reco.write(final_df.to_string())
print(final_df.head(20))

