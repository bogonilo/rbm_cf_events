import tensorflow.compat.v1 as tf
import numpy as np

class RBM:
    def __init__(self, visibleUnits, hiddenUnits, batchsize):
        tf.disable_v2_behavior()
        self.visibleUnits = visibleUnits
        self.hiddenUnits = hiddenUnits
        self.batchsize = batchsize
        self._w = np.zeros([self.visibleUnits, self.hiddenUnits], np.float32)
        self._vb = np.zeros([self.visibleUnits], np.float32)
        self._hb = np.zeros([self.hiddenUnits], np.float32)

    def train(self, trX):
        """GIBBS SAMPLING FOR CD-1"""
        # Phase 1: Input Processing
        vb = tf.placeholder("float", [self.visibleUnits], name="vb")  # visible bias vector
        hb = tf.placeholder("float", [self.hiddenUnits], name="hb")  # hidden bias vector
        W = tf.placeholder("float", [self.visibleUnits, self.hiddenUnits], name="W")  # weight matrix visib x hidd
        v0 = tf.placeholder("float", [None, self.visibleUnits], name="v0")
        _h0 = tf.nn.sigmoid(tf.matmul(v0, W) + hb)  # calculating probabilities of turning hidden units on
        h0 = tf.nn.relu(tf.sign(_h0 - tf.random_uniform(tf.shape(_h0))))  # drawing a sample from the distribution
        # Phase 2: Reconstruction
        _v1 = tf.nn.sigmoid(
            tf.matmul(h0, tf.transpose(W)) + vb)  # calculating probabilities of turning visible units on
        v1 = tf.nn.relu(tf.sign(_v1 - tf.random_uniform(tf.shape(_v1))))  # sampling from visible units distribution
        h1 = tf.nn.sigmoid(tf.matmul(v1, W) + hb)  # correspondent hidden units
        """CONTRASTIVE DIVERGENCE"""
        alpha = 1.0
        w_pos_grad = tf.matmul(tf.transpose(v0), h0)
        w_neg_grad = tf.matmul(tf.transpose(v1), h1)
        CD = (w_pos_grad - w_neg_grad) / tf.to_float(tf.shape(v0)[0])
        update_w = W + alpha * CD
        update_vb = vb + alpha * tf.reduce_mean(v0 - v1, 0)
        update_hb = hb + alpha * tf.reduce_mean(h0 - h1, 0)
        err = v0 - v1
        err_sum = tf.reduce_mean(err * err)
        """Variable initialisations"""
        cur_w = np.zeros([self.visibleUnits, self.hiddenUnits], np.float32)
        cur_vb = np.zeros([self.visibleUnits], np.float32)
        cur_hb = np.zeros([self.hiddenUnits], np.float32)
        prv_w = np.zeros([self.visibleUnits, self.hiddenUnits], np.float32)
        prv_vb = np.zeros([self.visibleUnits], np.float32)
        prv_hb = np.zeros([self.hiddenUnits], np.float32)
        sess = tf.Session()
        sess.run(tf.global_variables_initializer())
        epochs = 1000
        batchsize = self.batchsize
        errors = []
        for i in range(epochs):
            for start, end in zip(range(0, len(trX), batchsize), range(batchsize, len(trX), batchsize)):
                batch = trX[start:end]
                # print(start, end)
                # print(batch.shape, prv_hb.shape)
                cur_w = sess.run(update_w, feed_dict={v0: batch, W: prv_w, vb: prv_vb, hb: prv_hb})
                cur_vb = sess.run(update_vb, feed_dict={v0: batch, W: prv_w, vb: prv_vb, hb: prv_hb})
                cur_hb = sess.run(update_hb, feed_dict={v0: batch, W: prv_w, vb: prv_vb, hb: prv_hb})
                prv_w = cur_w
                prv_vb = cur_vb
                prv_hb = cur_hb
            errors.append(sess.run(err_sum, feed_dict={v0: trX, W: cur_w, vb: cur_vb, hb: cur_hb}))
            self._hb = cur_hb
            self._vb = cur_vb
            self._w = cur_w
            # print(errors[-1])

    def inference_recomm(self, inputUser):
        inputUser = inputUser.reshape(1, -1)
        vb = tf.placeholder("float", [self.visibleUnits], name="vb")  # visible bias vector
        hb = tf.placeholder("float", [self.hiddenUnits], name="hb")  # hidden bias vector
        W = tf.placeholder("float", [self.visibleUnits, self.hiddenUnits], name="W")  # weight matrix visib x hidd
        v0 = tf.placeholder("float", [None, self.visibleUnits], name="v0")
        # prv_w = np.zeros([self.visibleUnits, self.hiddenUnits], np.float32)
        # # Previous visible unit biases
        # prv_vb = np.zeros([self.visibleUnits], np.float32)
        # # Previous hidden unit biases
        # prv_hb = np.zeros([self.hiddenUnits], np.float32)
        sess = tf.Session()
        sess.run(tf.global_variables_initializer())
        hh0 = tf.nn.sigmoid(tf.matmul(v0, W) + hb)
        vv1 = tf.nn.sigmoid(tf.matmul(hh0, tf.transpose(W)) + vb)

        feed = sess.run(hh0, feed_dict={v0: inputUser, W: self._w, hb: self._hb})
        rec = sess.run(vv1, feed_dict={hh0: feed, W: self._w, vb: self._vb})
        return rec[0]
