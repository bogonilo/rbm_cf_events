"""handler URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore
from initial_data.models.user import User
from initial_data.models.event import Event
import mysql.connector as mysqlc

urlpatterns = [
    path('rest/', include('rest.urls')),
    path('admin/', admin.site.urls),
]

def get_db_connection():
    return mysqlc.connect(
        host="localhost",
        user="root",
        passwd="qwertyuiop4!A",
        database="rbm_rs_db",
        auth_plugin='mysql_native_password'
        )


# Use a service account
cred = credentials.Certificate('/home/lorenzo/Desktop/Collegamento a Università/Magistrale/Thesis/rbm_cf_events/credentials/connective-cp-firebase-adminsdk-22237-1bf8e3cb67.json')
firebase_admin.initialize_app(cred)
# firebase_admin.initialize_app(name='urls23353')

db = firestore.client()
# print('Test one time execution startup')
#
# Create a callback on_snapshot function to capture changes
def on_snapshot_callback_users(doc_snapshot, changes, read_time):
    for doc in doc_snapshot:
        user = User.create_user_from_doc(doc)
        print("doc snapshot for user " + user.name)
        user.add_user_to_csv()
        user.add_user_to_db(get_db_connection())
        user.calculate_relationships(get_db_connection())

def on_snapshot_callback_events(doc_snapshot, changes, read_time):
    for change in changes:
        print(change.type.name)
        if change.type.name == 'ADDED':
            event = Event.create_event_from_doc(change.document)
            event.add_event_to_csv()
            event.add_event_to_db(get_db_connection())
            event.calculate_cold_start_values(get_db_connection())
        elif change.type.name == 'REMOVED':
            event = Event.create_event_from_doc(change.document)
            event.remove_from_db(get_db_connection())
        else:
            print('MODIFIED')


doc_ref_users = db.collection('users')
# Watch the document
doc_watch_users = doc_ref_users.on_snapshot(on_snapshot_callback_users)

doc_ref_events = db.collection('ideas')
doc_watch_events = doc_ref_events.on_snapshot(on_snapshot_callback_events)