from django.db import models
import mysql.connector as mysqlc
from rs.rs import update_rbm_recommendations
import asyncio

# Create your models here.

def connect_to_db():
    return mysqlc.connect(
        host="localhost",
        user="root",
        passwd="qwertyuiop4!A",
        database="rbm_rs_db",
        auth_plugin='mysql_native_password'
    )

def get_recommendations(uid):
    mydb = connect_to_db()
    recommendation_query = "SELECT eventId FROM recommendation WHERE userId='"+str(uid)+"' ORDER BY recommendation DESC"
    cursor = mydb.cursor()
    cursor.execute(recommendation_query)
    data = []
    for item in cursor.fetchall():
        data.append(item[0])
    cursor.close()
    mydb.close()
    return {'data': data}

def add_participation(userId, eventId, participation):
    mydb = connect_to_db()
    participation_query = "UPDATE participation SET participation = "+participation+" WHERE userId = '"+userId+"' AND eventId = '"+eventId+"'"
    cursor = mydb.cursor()
    cursor.execute(participation_query)
    mydb.commit()
    cursor.close()
    cursor_participations = mydb.cursor()
    query_participations = "SELECT eventId FROM participation WHERE userId='"+userId+"' AND participation=1"
    cursor_participations.execute(query_participations)
    cursor_participations.fetchall()
    if(cursor_participations.rowcount > 2):
        print("updating recommendations with rbm")
        cursor_participations.close()
        mydb.close()
        update_rbm_recommendations(userId)
    else:
        cursor_participations.close()
        mydb.close()
