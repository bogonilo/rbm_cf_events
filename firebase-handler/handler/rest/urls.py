from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('recommendations', views.recommendation, name='recommendation'),
    path('participation', views.participation, name='participation'),
]