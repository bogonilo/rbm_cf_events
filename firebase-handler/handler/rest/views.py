from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from .models import get_recommendations, add_participation
from rs.rs import update_rbm_recommendations

def index(request):
    return HttpResponse("Hello, world. You're at the rest index.")


@csrf_exempt
def recommendation(request):
    if(request.method == "GET"):
        return HttpResponse()
    else:
        data = get_recommendations(request.POST.get("uid", ""))
        return JsonResponse(data)


@csrf_exempt
def participation(request):
    if(request.method == "POST"):
        userId = request.POST.get("uid", "")
        eventId = request.POST.get("eventId", "")
        participation_value = request.POST.get("participation", "")
        add_participation(userId, eventId, participation_value)
        return HttpResponse()
    else:
        return HttpResponse()
