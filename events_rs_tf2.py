import tensorflow as tf
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

pd.options.display.width = 0

"""GETTING ALL THE INPUT DATAS"""
ratings_df = pd.read_csv('data/data_csv/3ue_participations.csv', sep=':', header=None, engine='python')
events_df = pd.read_csv('data/data_csv/events.csv', sep=':', header=None, engine='python')
users_df = pd.read_csv('data/data_csv/users.csv', sep=':', header=None, engine='python')
ratings_df.columns = ['UserID', 'EventID', 'Participation']
events_df.columns = ['EventID', 'OrganizerID', 'OfficeTarget','Categories']
users_df.columns = ['UserID', 'Office', 'Chapter', 'Interests']

user_participation_df = ratings_df.pivot(index='UserID', columns='EventID', values='Participation')
print(user_participation_df.head())

trX = user_participation_df.values
index = user_participation_df.index
trX = tf.cast(trX, dtype=tf.float32)
print(trX.dtype)

"""SETTING UP RBM PARAMETERS:
Number of hidden units: hiddenUnits
Number of visible units: visibleUnits
Bias for visible units: vb
Bias for hidden units: hb
Weight matrix: W
"""
hiddenUnits = 30 #Number of features we're going to learn
visibleUnits = len(user_participation_df.columns) # 400 Number of unique events
alpha = 1.0

"""GIBBS SAMPLING FOR CD-1"""
#Phase 1: Input Processing
# v0 = tf.placeholder("float", [None, visibleUnits], name="v0")
def draw_sample_h0(v0, W, hb): #h0
    h0_prob = tf.nn.sigmoid(tf.matmul(v0, W) + hb)
    return tf.nn.relu(tf.sign(h0_prob - tf.random.uniform(tf.shape(h0_prob)))) # drawing a sample from the distribution

#Phase 2: Reconstruction
def draw_sample_v1(h0, W, vb): #v1
    v1_prob = tf.nn.sigmoid(tf.matmul(h0, tf.transpose(W)) + vb)
    return tf.nn.relu(tf.sign(v1_prob - tf.random.uniform(tf.shape(v1_prob)))) # sampling from visible units distribution

def calculate_h1(v1,W,hb): #h1
    return tf.nn.sigmoid(tf.matmul(v1, W) + hb) # correspondent hidden units

def calculate_error_sum(v0,v1): #err_sum
    err = v0 - v1
    return tf.reduce_mean(err * err)

def calculate_CD(v0,h0,v1,h1): #CD
    w_pos_grad = tf.matmul(tf.transpose(v0), h0)
    w_neg_grad = tf.matmul(tf.transpose(v1), h1)
    # Calculate the Contrastive Divergence to maximize
    return (w_pos_grad - w_neg_grad) / tf.cast(tf.shape(v0)[0], dtype=tf.float32)

"""Variable initialisations"""
#Current weight
cur_w = np.zeros([visibleUnits, hiddenUnits], np.float32)
#Current visible unit biases
cur_vb = np.zeros([visibleUnits], np.float32)
#Current hidden unit biases
cur_hb = np.zeros([hiddenUnits], np.float32)
#Previous weight
prv_w = np.zeros([visibleUnits, hiddenUnits], np.float32)
#Previous visible unit biases
prv_vb = np.zeros([visibleUnits], np.float32)
#Previous hidden unit biases
prv_hb = np.zeros([hiddenUnits], np.float32)

epochs = 1000
batchsize = 25
errors = []
for i in range(epochs):
    for start, end in zip( range(0, len(trX), batchsize), range(batchsize, len(trX), batchsize)):
        batch = trX[start:end]
        v0 = batch
        h0 = draw_sample_h0(v0, cur_w, cur_hb)
        v1 = draw_sample_v1(h0, cur_w, cur_vb)
        h1 = calculate_h1(v1, cur_w, cur_hb)
        CD = calculate_CD(v0,h0,v1,h1)
        cur_w = prv_w + alpha * CD
        cur_vb = prv_vb + alpha * tf.reduce_mean(v0 - v1, 0)
        cur_hb = prv_hb + alpha * tf.reduce_mean(h0 - h1, 0)
        prv_w = cur_w
        prv_vb = cur_vb
        prv_hb = cur_hb
    errors.append(calculate_error_sum(trX, draw_sample_v1(draw_sample_h0(trX, cur_w, cur_hb), cur_w, cur_vb)))
    print (errors[-1])
plt.plot(errors)
plt.ylabel('Error')
plt.xlabel('Epoch')
plt.show()

"""TRAINING ENDED"""

"""INFEERNCE TIME:)"""
#Selecting the input user
inputUser = tf.reshape(trX[0], [1,-1])
print(inputUser)

feed = tf.nn.sigmoid(tf.matmul(inputUser, prv_w) + prv_hb)
rec = tf.nn.sigmoid(tf.matmul(feed, tf.transpose(prv_w)) + prv_vb)
# print("------ rec--------")
# print(rec)
# rec2_h0 = draw_sample_h0(inputUser,prv_w,prv_hb)
# rec2 = draw_sample_v1(rec2_h0, prv_w, prv_vb)
# print("------ rec2 ------")
# print(rec2)
scored_events_df_mock = events_df[events_df['EventID'].isin(user_participation_df.columns)]
scored_events_df_mock = scored_events_df_mock.assign(RecommendationScore=rec[0])
scored_events_df_mock = scored_events_df_mock[scored_events_df_mock['EventID']>299]

sel_user_participations_df = ratings_df[ratings_df['UserID'] == 0]

merged_df = pd.merge(sel_user_participations_df, scored_events_df_mock, how='inner', on='EventID')
# merged_df = pd.merge(sel_user_non_part_events_df, scored_events_df_mock, how='inner', on='EventID')
final_df = merged_df.sort_values(['RecommendationScore'], ascending=False).head(20)
with open('recommendation_score_tf2.0.txt', 'w') as reco:
    reco.write(final_df.to_string())
print(final_df.head(20))