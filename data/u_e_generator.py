"""
user-event relationship vector generator
format:
userId:eventId:partecipation:organizer's office affinity,organizer's role/chapter affinity,event target affinity,category/interests matching
userId : 'u'+number
eventId : 'e'+number
partecipation, organizer's office affinity, organizer's role/chapter affinity, event target affinity, category/interests matching: bin value
"""
import pandas as pd
import csv
import random as rd

data_events = pd.read_csv("data_csv/events.csv", delimiter=':', header=None, names=['eventId','organizerId','officeTarget','categories'])
data_users = pd.read_csv("data_csv/users.csv", delimiter=':', header=None, names=['userId','office','chapter','interests'])
participation_opt = ['0', '1']


with open('data_csv/3ue_relationships.csv', 'w') as ue_relationships:
    writer = csv.writer(ue_relationships, delimiter=':', quotechar='"')
    for index, user in data_users.iterrows():
        user_interests = user['interests'].split(',')
        if(index % 5 == 0):
            print('ue_relationship generated for: '+str(index*100/200)+'% users')
        for index_e, event in data_events.iterrows():
            organizerId = event['organizerId']
            organizer = data_users[data_users.userId == organizerId]
            event_categories = event['categories'].split(',')
            organizer_location_match = 0
            chapter_match = 0
            target_match = 0
            match_interests = 0
            if(organizer['office'].values[0] == user['office']):
                organizer_location_match = 1
            else:
                organizer_location_match = 0
            if(organizer['chapter'].values[0] == user['chapter']):
                chapter_match = 1
            else:
                chapter_match = 0
            if(event['officeTarget'] == user['office']):
                target_match = 1
            else:
                target_match = 0
            matching_interests = set(event_categories) & set(user_interests)
            # if(len(matching_interests) > 0 and target_match==1): # 2ue
            if(len(matching_interests) > 0):
                match_interests = 1
                # participation = 1      #1ue & 2ue
            else:
                match_interests = 0
                # participation = 0      #1ue & 2ue

            if (target_match == 0):
                participation = 0
            else:
                if(index == 0 and index_e > 299):
                    participation = 0
                else:
                    if(organizer_location_match + chapter_match + match_interests >= 2):
                        participation = 1
                    else:
                        participation = 0
            # participation = rd.sample(participation_opt, k=1)[0]    # 0ue
            if(organizerId == user["userId"]):
                participation = 1
            # if (index == 0 and index_e > 299):
            #     participation = 0
            relationship_vector = str(organizer_location_match) + ',' + str(chapter_match) + ',' + str(target_match) + ',' + str(match_interests)
            writer.writerow([user['userId'], event['eventId'], str(participation), relationship_vector])
    # for index, event in data_events.iterrows():
    #     organizerId = event['organizerId']
    #     organizer = data_users[data_users.userId == organizerId]
    #     if(index % 5 == 0):
    #         print('ue_relationship generated for: '+str(index)+' events')
    #     event_categories = event['categories'].split(',')
    #     for index_u, user in data_users.iterrows():
    #         user_interests = user['interests'].split(',')
    #         if(organizer['office'].values[0] == user['office']):
    #             location_match = 1
    #         else:
    #             location_match = 0
    #         if(organizer['chapter'].values[0] == user['chapter']):
    #             chapter_match = 1
    #         else:
    #             chapter_match = 0
    #         if(event['officeTarget'] == user['office']):
    #             target_match = 1
    #         else:
    #             target_match = 0
    #         if(target_match+chapter_match+location_match<=1):
    #             partecipation = 0
    #         else:
    #             partecipation = 1
    #         matching_interests = set(event_categories) & set(user_interests)
    #         if(len(matching_interests) > 0):
    #             match_interests = 1
    #         else:
    #             match_interests = 0
    #         relationship_vector = [ partecipation, location_match, chapter_match, target_match, match_interests ]
    #         writer.writerow([user['userId'], event['eventId'], relationship_vector])
    ue_relationships.close()
