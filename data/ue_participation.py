import pandas as pd
import csv

data_ue_rel = pd.read_csv("data_csv/3ue_relationships.csv", delimiter=':', header=None, names=['userId', 'eventId', 'participation', 'relationshipVector'])

with open('data_csv/3ue_participations.csv', 'w') as ue_participations:
    writer = csv.writer(ue_participations, delimiter=':', quotechar='"')
    for index, ue_rel in data_ue_rel.iterrows():
        relationship_vector = ue_rel['participation']
        writer.writerow([ue_rel['userId'], ue_rel['eventId'], relationship_vector])
        if(index % 500 == 0):
            print(str(index/800) + '%')
    ue_participations.close()
