# user_data_template = {
#     'bio' : '',
#     'chapter' : 'Business Analyst/Developer',
#     'email' : 'mockup.user@accenture.com',
#     'interests' : ['Food & Drink', 'Party', 'Culture & Art'],
#     'joinedIdeas' : [],
#     'name' : 'Mockup User',
#     'office' : 'Amsterdam (ITO, Building 01)',
#     'phoneN' : "",
#     'position' : 'Analyst',
#     'profileImgUrl' : 'https://firebasestorage.googleapis.com/v0/b/connective-dev.appspot.com/o/profile_images%2Faccenture_logo.png?alt=media&token=81931cf2-8c64-4ee7-a9d7-0b5e4bd11249',
#     'skills' : ['jira','confluence','power point'],
#     'uid' : 'sfGOIcwnUZWJtHIxVKzKa4HxMDr2'
# }
#
# user_data_empty = {
#     'bio' : '',
#     'chapter' : '',
#     'email' : '',
#     'interests' : [],
#     'joinedIdea' : [],
#     'name' : '',
#     'office' : '',
#     'phoneN' : '',
#     'position' : '',
#     'profileImg' : 'https://firebasestorage.googleapis.com/v0/b/connective-dev.appspot.com/o/profile_images%2Faccenture_logo.png?alt=media&token=81931cf2-8c64-4ee7-a9d7-0b5e4bd11249',
#     'skills' : [],
#     'uid' : ''
# }

'''
users csv format:
userId:office:chapter:[interests]
'''

import csv
import random as rd

offices = ['Amsterdam (ITO, Building 01)', 'Groningen', 'Malaga']
chapters = ['Android Development', 'iOS Team', 'Business Analyst/Developer', 'UX Design']
interests = ['Food & Drink','Party','Culture & Art','Networking','Volunteering','Outside Activity','Sport & Wellness',
'Travel','Game','Career & Business','Learning','Music']
n_of_users = 200
with open('data_csv/users.csv', mode='w') as users_csv:
    writer = csv.writer(users_csv, delimiter=':', quotechar='"')
    for user in range(0, n_of_users):
        office = rd.choice(offices)
        chapter = rd.choice(chapters)
        interest = rd.sample(interests, k=3)
        writer.writerow([str(user), str(office), str(chapter), str(interest[0]+','+interest[1]+','+interest[2])])
        # writer.writerow(['u'+str(user), str(office), str(chapter), str(interest[0]+','+interest[1]+','+interest[2])])
    users_csv.close()