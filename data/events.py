'''
events csv format:
eventId:organizerId:officeTarget:categories
'''

import csv
import random as rd
import pandas as pd

offices = ['Amsterdam (ITO, Building 01)', 'Groningen', 'Malaga']
# chapters = ['Android Development', 'iOS Team', 'Business Analyst/Developer', 'UX Design']
interests = ['Food & Drink','Party','Culture & Art','Networking','Volunteering','Outside Activity','Sport & Wellness',
'Travel','Game','Career & Business','Learning','Music']

data_users = pd.read_csv("data_csv/users.csv", delimiter=':', header=None, names=['userId','office','chapter','interests'])

n_of_events = 400
n_of_users = 200
users = []
for user in range(0, n_of_users):
    users.append(str(user))
    # users.append('u'+str(user))
# creators = rd.sample(users, k=40) #picking only 40 users to be the creators of events
creators = data_users.sample(40) #picking only 40 users to be the creators of events

print('users ids: '+ str(users))
print('creators ids: '+ str(creators))
with open('data_csv/events.csv', mode='w') as events_csv:
    writer = csv.writer(events_csv, delimiter=':', quotechar='"')
    for event in range(0, n_of_events):
        id = str(event)
        # id = 'e' + str(event)
        organizer = creators.sample(1)
        office_target = organizer["office"].values[0]
        interest = rd.sample(interests, k=3)
        writer.writerow([id, organizer["userId"].values[0], office_target, str(interest[0]+','+interest[1]+','+interest[2])])
    events_csv.close()