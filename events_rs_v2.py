import tensorflow.compat.v1 as tf
#Numpy contains helpful functions for efficient mathematical calculations
import numpy as np
#Dataframe manipulation library
import pandas as pd
#Graph plotting library
import matplotlib.pyplot as plt

pd.options.display.width = 0
tf.disable_v2_behavior()

relationships_df = pd.read_csv('data/data_csv/ue_relationships.csv', sep=':', header=None, engine='python')
ratings_df = pd.read_csv('data/data_csv/ue_participations.csv', sep=':', header=None, engine='python')
events_df = pd.read_csv('data/data_csv/events.csv', sep=':', header=None, engine='python')
users_df = pd.read_csv('data/data_csv/users.csv', sep=':', header=None, engine='python')
relationships_df.columns = ['UserID', 'EventID', 'Participation', 'Relationship']
ratings_df.columns = ['UserID', 'EventID', 'Participation']
events_df.columns = ['EventID', 'OrganizerID', 'OfficeTarget','Categories']
users_df.columns = ['UserID', 'Office', 'Chapter', 'Interests']
# print(ratings_df.head())

user_participation_df = ratings_df.pivot(index='UserID', columns='EventID', values='Participation')
user_relationship_df = relationships_df.pivot(index='UserID', columns='EventID', values='Relationship')
print(user_participation_df.head(5))
print(user_relationship_df.head(5))

trX = user_participation_df.values
index = user_participation_df.index
rel_matrix = user_relationship_df.values

# print(np.array(rel_matrix[0]))

def rel_array_to_values(rel_array):
    vector = []
    for item in rel_array:
        values = item.split(',')
        value = (float(values[0]) + float(values[1]) + float(values[2]) + float(values[3])) / 4.0
        vector.append(value)
    return vector

# changing relationship matrix with normalized values
for i in range(len(rel_matrix)):
    rel_matrix[i] = rel_array_to_values(np.array(rel_matrix[i]))
# print(rel_matrix.shape, tf.transpose(rel_matrix).shape)

result = (rel_matrix + trX) / 2.0
# result = tf.divide((relationship_matrix + v0),2.0)
# sess = tf.Session()
# sess.run(tf.global_variables_initializer())
# sess.run(result, feed_dict={relationship_matrix: rel_matrix[0:100], v0:trX[0:100]})
# print(result[0])

hiddenUnits = 30
visibleUnits = len(user_participation_df.columns)
vb = tf.placeholder("float", [visibleUnits]) #Number of unique events
hb = tf.placeholder("float", [hiddenUnits]) #Number of features we're going to learn
W = tf.placeholder("float", [visibleUnits, hiddenUnits]) #weight matrix vb x hb

#Phase 1: Input Processing
v0 = tf.placeholder("float", [None, visibleUnits])
# calculating probabilities of turning hidden units on
# here the relationship matrix is taken into account
_h0 = tf.nn.sigmoid(tf.matmul(v0, W) + hb)
h0 = tf.nn.relu(tf.sign(_h0 - tf.random_uniform(tf.shape(_h0)))) # drawing a sample from the distribution
#Phase 2: Reconstruction
_v1 = tf.nn.sigmoid(tf.matmul(h0, tf.transpose(W)) + vb)  # calculating probabilities of turning visible units on
v1 = tf.nn.relu(tf.sign(_v1 - tf.random_uniform(tf.shape(_v1)))) # sampling from visible units distribution
h1 = tf.nn.sigmoid(tf.matmul(v1, W) + hb) # correspondent hidden units

# CONTRASTIVE DIVERGENCE
#Learning rate
alpha = 1.0
#Create the gradients
w_pos_grad = tf.matmul(tf.transpose(v0), h0)
w_neg_grad = tf.matmul(tf.transpose(v1), h1)
#Calculate the Contrastive Divergence to maximize
CD = (w_pos_grad - w_neg_grad) / tf.to_float(tf.shape(v0)[0])
#Create methods to update the weights and biases
update_w = W + alpha * CD
update_vb = vb + alpha * tf.reduce_mean(v0 - v1, 0)
update_hb = hb + alpha * tf.reduce_mean(h0 - h1, 0)

err = v0 - v1
err_sum = tf.reduce_mean(err * err)

#Current weight
cur_w = np.zeros([visibleUnits, hiddenUnits], np.float32)
#Current visible unit biases
cur_vb = np.zeros([visibleUnits], np.float32)
#Current hidden unit biases
cur_hb = np.zeros([hiddenUnits], np.float32)
#Previous weight
prv_w = np.zeros([visibleUnits, hiddenUnits], np.float32)
#Previous visible unit biases
prv_vb = np.zeros([visibleUnits], np.float32)
#Previous hidden unit biases
prv_hb = np.zeros([hiddenUnits], np.float32)
sess = tf.Session()
sess.run(tf.global_variables_initializer())

epochs = 1000
batchsize = 50
errors = []
for i in range(epochs):
    for start, end in zip( range(0, len(result), batchsize), range(batchsize, len(result), batchsize)):
        batch = result[start:end]
        cur_w = sess.run(update_w, feed_dict={v0: batch, W: prv_w, vb: prv_vb, hb: prv_hb})
        cur_vb = sess.run(update_vb, feed_dict={v0: batch, W: prv_w, vb: prv_vb, hb: prv_hb})
        cur_hb = sess.run(update_hb, feed_dict={v0: batch, W: prv_w, vb: prv_vb, hb: prv_hb})
        prv_w = cur_w
        prv_vb = cur_vb
        prv_hb = cur_hb
    errors.append(sess.run(err_sum, feed_dict={v0: result, W: cur_w, vb: cur_vb, hb: cur_hb}))
    print (errors[-1])
plt.plot(errors)
plt.ylabel('Error')
plt.xlabel('Epoch')
plt.show()

#Selecting the input user
inputUser = result[0].reshape(1, -1)
i_u_rel_matrix = rel_matrix[0].reshape(1,-1)
print(inputUser)
print(i_u_rel_matrix)

tf.print(W, output_stream="file:///Users/lorenzo.bogoni/Desktop/Thesis/rbm_cf_events/weight.txt")

#Feeding in the user and reconstructing the input
hh0 = tf.nn.sigmoid(tf.matmul(v0, W) + hb)
vv1 = tf.nn.sigmoid(tf.matmul(hh0, tf.transpose(W)) + vb)
feed = sess.run(hh0, feed_dict={v0: inputUser, W: prv_w, hb: prv_hb})
rec = sess.run(vv1, feed_dict={hh0: feed, W: prv_w, vb: prv_vb})
# print(rec)

scored_events_df_mock = events_df[events_df['EventID'].isin(user_participation_df.columns)]
scored_events_df_mock = scored_events_df_mock.assign(RecommendationScore=rec[0])
scored_events_df_mock = scored_events_df_mock[scored_events_df_mock['EventID']>299]
# print(scored_events_df_mock.sort_values(["RecommendationScore"], ascending=False).head(20))
# with open('recommendation_score_tf_dl_v2.txt', 'w') as reco:
#     reco.write(scored_events_df_mock.sort_values(["RecommendationScore"], ascending=False).to_string())

sel_user_participations_df = ratings_df[ratings_df['UserID'] == 0]
sel_user_participations_df["RelationshipValue"] = rel_matrix[0]
# print(sel_user_participations_df.to_string())
# sel_user_non_part_events_df = sel_user_participations_df[sel_user_participations_df['Participation'] == 0]

merged_df = pd.merge(sel_user_participations_df, scored_events_df_mock, how='inner', on='EventID')
final_df = merged_df.sort_values(['RecommendationScore'], ascending=False).head(20)
final_df.drop(['OrganizerID'], axis=1)
with open('recommendation_score_tf_dl_v2.txt', 'w') as reco:
    reco.write(final_df.to_string())
print(final_df.head(20))
